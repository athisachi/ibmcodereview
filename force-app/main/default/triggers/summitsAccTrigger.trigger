trigger summitsAccTrigger on Account (before insert, after update, before update) {

    // Only run for North American Countries, we want to exclude international countries for now
    List<Account> northAmericanAcc = new List<Account>();
    getNorthAmericanAccs();
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {

        } else if (Trigger.isUpdate) {
            summitsAccTriggerHandler.beforeUpdate(northAmericanAcc);
        }
    }
    if(Trigger.isAfter){
        if (Trigger.isInsert) {
            summitsAccTriggerHandler.afterInsert(northAmericanAcc);
        } else if (Trigger.isUpdate) {
            summitsAccTriggerHandler.afterUpdate(northAmericanAcc);
        }
    }
    
    private static void getNorthAmericanAccs(){
        for(Account a : Trigger.new){
            if(a.BillingCountry != 'United States' || a.BillingCountry != 'Canada' || a.BillingCountry != 'Mexico'  ){
                northAmericanAcc.add(a);
            }
        }
    }
}